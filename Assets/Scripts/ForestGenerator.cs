﻿/**
 * Script: ForestGenerator.cs
 * Author: Matthew Wong
 * Desc: This script takes two points and makes a square area. It fills the square area randomly with trees, plants and grasses prefabs. 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ForestGenerator : MonoBehaviour {

	class Pair{
		float x;
		float y;

		public Pair(float x, float y){
			this.x = x;
			this.y = y;
		}

		public float getY(){
			return y;
		}

		public float getX(){
			return x;

		}
	}

	//two points to determine the square area
	public Transform point1;
	public Transform point2;

	//prefix or "key word" to find all the correct prefabs
	[Header("Prefixes")]
	public string treePrefix;
	public string plantsPrefix;
	public string grassPrefix;
	[Space(10)]

	//databases for all the prefabs' locations
	private List<string> trees;
	private List<string> plants;
	private List<string> grasses;

	//information about the square area
	private Pair[,] plain;
	private float startX;
	private float startZ;
	private float endX;
	private float endZ;

	private float y;

	[Header("Density")]
	public int areaDivision; //determines how large to divide the square area

	//determines how many of each item spawns per item
	public int treeDensity; 
	public int plantDensity;
	public int grassDensity;

	//determines space around the target area to randomly place
	public int offsetX;
	public int offsetZ;

	[Space(10)]
	[Header("Path")]
	//directory of where to find the prefabs
	public string assetPath;
	public string rootFolder;

	// Use this for initialization
	void Start () {
		if (point1 != null && point2 != null) {

			trees = new List<string> ();
			plants = new List<string> ();
			grasses = new List<string> ();

			//sets the starting positions and builds the plain
			startX = point1.position.x;
			startZ = point1.position.z;
			endX = point2.position.x;
			endZ = point2.position.z;

			//gets the average position of the y plain
			float min = Mathf.Min (point1.position.y, point2.position.y);
			float max = Mathf.Max (point1.position.y, point2.position.y);
			y = (max - min) + min;

			int xDeterminant = Mathf.RoundToInt ((endX - startX) / areaDivision);
			int zDeterminant = Mathf.RoundToInt ((endZ - startZ) / areaDivision);

			plain = new Pair[xDeterminant, zDeterminant];

			float xTrker = startX;
			float zTrker = startZ;
			for (int i = 0; i < plain.GetLength (0); i++) {
				if (i != 0)
					xTrker += xDeterminant;
				zTrker = startZ;
				for (int a = 0; a < plain.GetLength (1); a++) {
					if (a != 0)
						zTrker += zDeterminant;

					if (xTrker > endX)
						xTrker = startX;
					if (zTrker > endZ)
						zTrker = startZ;
					
					plain [i, a] = new Pair (xTrker, zTrker); 
				}
			}

			DirectoryInfo dir = new DirectoryInfo (assetPath);
			FileInfo[] info = dir.GetFiles ();

			foreach (FileInfo file in info) {
				
				if(file.Name.Contains(treePrefix) && !file.Name.Contains("meta"))
					trees.Add(file.FullName);
				if (file.Name.Contains (plantsPrefix) && !file.Name.Contains("meta"))
					plants.Add (file.FullName);
				if (file.Name.Contains (grassPrefix) && !file.Name.Contains("meta"))
					grasses.Add (file.FullName);
			}

			//generates 
			generate (trees, treeDensity);
			generate (plants, plantDensity);
			generate (grasses, grassDensity);
		}
	}
	
	// Update is called once per frame
	void Update () {}

	private void generate(List<string> prefabs, int density){
		//loops through the entire area
		for (int i = 0; i < plain.GetLength (0); i++) {
			for (int a = 0; a < plain.GetLength (1); a++) {
				//gets the starting position
				float x = plain [i, a].getX();
				float z = plain [i, a].getY();

				for (int b = 0; b < density; b++) {
					//randomly chooses a position
					float xPlace = Random.Range (x, x + offsetX);
					float zPlace = Random.Range (z, z + offsetZ);
					//randomly chooses an object from the list and instantiate
					string objectPath = "";
					objectPath = prefabs [Random.Range (0, prefabs.Count)];
					objectPath = objectPath.Substring(objectPath.IndexOf(rootFolder), objectPath.IndexOf(".prefab") - objectPath.IndexOf(rootFolder));
					objectPath = objectPath.Replace ("\\", "/");
					Debug.Log ("X: " + xPlace);
					Debug.Log ("Z: " + zPlace);
					Instantiate (Resources.Load (objectPath), new Vector3(xPlace, y, zPlace), Quaternion.identity);
				}
			}
		}

	}
}
